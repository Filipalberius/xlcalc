package controller.readwrite;

import model.Address;
import model.Slot;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Saves a file in the form of Map Entry sets.
 */
public class XLPrintStream extends PrintStream {
    public XLPrintStream(String fileName) throws FileNotFoundException {
        super(fileName);
    }

    public void save(Set<Entry<Address, Slot>> set) {
        for (Entry<Address, Slot> entry : set) {
            print(entry.getKey().toString());
            print('=');
            println(entry.getValue().toString());
        }
        flush();
        close();
    }
}
