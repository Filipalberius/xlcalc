package controller.readwrite;

import util.XLException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;

/**
 * Reads a file, saving its content into a Map.
 */
public class XLBufferedReader extends BufferedReader {
    public XLBufferedReader(String name) throws FileNotFoundException {
        super(new FileReader(name));
    }

    public void load(Map<String, String> map) {
        try {
            while (ready()) {
                String string = readLine();
                while(string != null) {
                    int i = string.indexOf("=");
                    map.put(string.substring(0, i), string.substring(i + 1));
                    string = readLine();
                }
            }
        } catch (Exception e) {
            throw new XLException(e.getMessage());
        }
    }
}
