package controller;

import gui.*;
import gui.menu.ClearAllMenuItem;
import gui.menu.ClearMenuItem;
import gui.menu.LoadMenuItem;
import gui.menu.NewMenuItem;
import gui.menu.SaveMenuItem;
import model.SpreadSheet;

/**
 * The connector between the view and the model. Contains the runnable Main method that starts the program. The idea
 * is simple: for every graphical element in the view that needs functionality, an observers is added that checks
 * if the graphical element is pressed.
 */
public class Communicator {
    private final int ROWS = 10;
    private final int COLS = 8;

    /**
     * When a new Communicator is called, a new window and a new SpreadSheet is created, and they are connected to
     * each other.
     */
    public Communicator() {
        XL xl = new XL(new XLList(), new XLCounter(), ROWS, COLS);
        SpreadSheet spreadSheet = new SpreadSheet(ROWS, COLS);
        addObserversToGui(spreadSheet, xl);
    }

    /**
     * This constructor assumes there already is an XL open. A new SpreadSheet and a new window with the same
     * properties as the old one are created and connected.
     * @param oldXL the previously opened window, whose properties are transferred to the new window
     */
    public Communicator(XL oldXL) {
        SpreadSheet spreadSheet = new SpreadSheet(oldXL.getRows(), oldXL.getCols());
        XL xl = new XL(oldXL);
        addObserversToGui(spreadSheet, xl);
    }

    //this method fetches all the graphical elements from the view that need to be added functionality,
    //and adds observers that implement the functionality. these observers might need access to other graphical elements
    //or the model to work, and therefore have those in their constructors.
    private void addObserversToGui(SpreadSheet spreadSheet, XL xl) {
        NewMenuItem newMenuItem = xl.getNewMenuItem();
        Editor editor = xl.getEditor();
        MarkedSlot markedSlot = xl.getMarkedSlot();
        StatusLabel statusLabel = xl.getStatusLabel();
        SlotLabels slotLabels = xl.getSlotLabels();
        LoadMenuItem loadMenuItem = xl.getLoadMenuItem();
        SaveMenuItem saveMenuItem = xl.getSaveMenuItem();
        ClearMenuItem clearMenuItem = xl.getClearMenuItem();
        ClearAllMenuItem clearAllMenuItem = xl.getClearAllMenuItem();

        editor.getObservable().addObserver(
                new EditorObserver(spreadSheet, statusLabel, markedSlot, slotLabels));
        markedSlot.addObserver(
                new MarkedSlotObserver(spreadSheet, editor));
        loadMenuItem.getObservableAttribute().addObserver(
                new LoadMenuItemObserver(spreadSheet, statusLabel, slotLabels));
        saveMenuItem.getObservableAttribute().addObserver(
                new SaveMenuItemObserver(spreadSheet, statusLabel));
        spreadSheet.addObserver(
                new ModelObserver(spreadSheet, slotLabels));
        clearMenuItem.getObservableAttribute().addObserver(
                new ClearMenuItemObserver(markedSlot, spreadSheet, slotLabels, statusLabel));
        clearAllMenuItem.getObservableAttribute().addObserver(
                new ClearAllMenuItemObserver(spreadSheet, slotLabels));
        newMenuItem.getObservableAttribute().addObserver(
                new NewMenuItemObserver(xl));
    }

    public static void main(String[] args) {
        new Communicator();
    }
}
