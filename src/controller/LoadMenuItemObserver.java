package controller;

import controller.readwrite.XLBufferedReader;
import gui.SlotLabels;
import gui.StatusLabel;
import model.*;
import util.XLException;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * The observers of the LoadMenuItem. After the Load menu is selected, this class opens a file and updates the model
 * given the file path. The StatusLabel is updated afterwards, containing error messages or confirmation that the file
 * was loaded.
 */
public class LoadMenuItemObserver implements Observer {
    private SpreadSheet spreadSheet;
    private StatusLabel statusLabel;
    private SlotLabels slotLabels;

    LoadMenuItemObserver(SpreadSheet spreadSheet, StatusLabel statusLabel, SlotLabels slotLabels) {
        this.spreadSheet = spreadSheet;
        this.statusLabel = statusLabel;
        this.slotLabels = slotLabels;
    }

    //creates an xlbufferedreader, gives it an empty map which gets filled with the file content. updates the
    //statuslabel whether it succeeded or not.
    @Override
    public void update(Observable observable, Object o) {
        String path = (String) o;
        try {
            XLBufferedReader xlBufferedReader = new XLBufferedReader(path);
            Map<String, String> map = new HashMap<>();
            xlBufferedReader.load(map);
            updateModel(map);
            statusLabel.setText(" " + path + " loaded");
        } catch (FileNotFoundException e) {
            int index = path.lastIndexOf('/');
            statusLabel.setText("Error opening file " + path.substring(index + 1));
        }
    }

    //clears the model and view, then updates the model. before the model is completely loaded, there might be
    //references to slots that have not yet been loaded. therefore it needs to call setSlotUnsafe(slot, address) so
    //the model doesnt check if it is valid or not. after the model is completely loaded, every slot is checked in the
    //spreadSheet.checkAll() method.
    private void updateModel(Map<String, String> map) {
        spreadSheet.clearAllSlots();
        slotLabels.clearAll();
        AddressFactory addressFactory = spreadSheet.getAddressFactory();
        SlotFactory slotFactory = spreadSheet.getSlotFactory();

        try {
            for (Map.Entry<String, String> mapEntry : map.entrySet()) {
                Address address = addressFactory.create(mapEntry.getKey());
                Slot slot = slotFactory.create(mapEntry.getValue());
                spreadSheet.setSlotUnsafe(slot, address);
            }
        } catch (XLException e) {
            statusLabel.setText("Error loading file: " + e.getMessage());
            return;
        }

        try {
            spreadSheet.checkAll();
            spreadSheet.setUpdated();
        } catch (XLException e) {
            statusLabel.setText("Error loading file: " + e.getMessage());
        }
    }
}
