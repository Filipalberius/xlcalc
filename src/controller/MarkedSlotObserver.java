package controller;

import gui.Editor;
import gui.MarkedSlot;
import model.AddressFactory;
import model.SpreadSheet;

import java.util.Observable;
import java.util.Observer;

/**
 * MarkedSlotObserver observes the instance of MarkedSlot in gui. When MarkedSlot is updated, this class gets notified
 * and fetches the marked Slot's content from the model and puts it in the editor.
 */
public class MarkedSlotObserver implements Observer {
    private SpreadSheet spreadSheet;
    private Editor editor;

    MarkedSlotObserver(SpreadSheet spreadSheet, Editor editor) {
        this.spreadSheet = spreadSheet;
        this.editor = editor;
    }

    //if markedslot is updated, editor is given the marked slots content.
    @Override
    public void update(Observable observable, Object o) {
        MarkedSlot marked = (MarkedSlot) observable;
        AddressFactory af = spreadSheet.getAddressFactory();
        editor.setText(spreadSheet.getSlot(af.create(marked.getAddress())).getContent());
    }
}
