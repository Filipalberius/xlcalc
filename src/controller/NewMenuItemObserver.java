package controller;

import gui.XL;

import java.util.Observable;
import java.util.Observer;

/**
 * Observes the New menu option. A new Communicator is created, based on the current program.
 */
public class NewMenuItemObserver implements Observer {
    private XL xl;

    NewMenuItemObserver(XL xl) {
        this.xl = xl;
    }

    //instantiates a new communicator based on the current xl window.
    @Override
    public void update(Observable observable, Object o) {
        new Communicator(xl);
    }
}
