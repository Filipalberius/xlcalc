package controller;

import gui.SlotLabels;
import model.SpreadSheet;

import java.util.Observable;
import java.util.Observer;

/**
 * This class observes the ClearAllMenuItem and gives it functionality. When the Clear All menu is pressed,
 * this class gets notified and clears all slots from both view and model.
 */
public class ClearAllMenuItemObserver implements Observer {
    private SpreadSheet spreadSheet;
    private SlotLabels slotLabels;

    ClearAllMenuItemObserver(SpreadSheet spreadSheet, SlotLabels slotLabels) {
        this.spreadSheet = spreadSheet;
        this.slotLabels = slotLabels;
    }

    @Override
    public void update(Observable observable, Object o) {
        spreadSheet.clearAllSlots();
        slotLabels.clearAll();
    }
}
