package controller;

import controller.readwrite.XLPrintStream;
import gui.StatusLabel;
import model.SpreadSheet;
import util.XLException;
import java.io.FileNotFoundException;
import java.util.Observable;
import java.util.Observer;

/**
 * Observes the Save menu option. When someone saves a file, finally this class is notified with the file path and the
 * file is saved.
 */
public class SaveMenuItemObserver implements Observer {
    private SpreadSheet spreadSheet;
    private StatusLabel statusLabel;

    SaveMenuItemObserver (SpreadSheet spreadSheet, StatusLabel statusLabel) {
        this.spreadSheet = spreadSheet;
        this.statusLabel = statusLabel;
    }

    //saves file to disk, and updates the statuslabel.
    @Override
    public void update(Observable observable, Object o){
        String file = (String) o;
        try {
            XLPrintStream printer = new XLPrintStream(file);
            printer.save(spreadSheet.getSlots().entrySet());
            statusLabel.setText(file + " saved");
        } catch (FileNotFoundException e){
            throw new XLException("file does not exist");
        }
    }
}
