package controller;

import gui.MarkedSlot;
import gui.SlotLabels;
import gui.StatusLabel;
import model.AddressFactory;
import model.SpreadSheet;
import util.XLException;
import java.util.Observable;
import java.util.Observer;

/**
 * The observers of the ClearMenuItem: when this is pressed, the selected slot (contained in the MarkedSlot class)
 * is cleared from model and view.
 */
public class ClearMenuItemObserver implements Observer {
    private MarkedSlot markedSlot;
    private SpreadSheet spreadSheet;
    private SlotLabels slotLabels;
    private StatusLabel statusLabel;

    ClearMenuItemObserver(MarkedSlot markedSlot, SpreadSheet spreadSheet,
                          SlotLabels slotLabels, StatusLabel statusLabel) {
        this.markedSlot = markedSlot;
        this.spreadSheet = spreadSheet;
        this.slotLabels = slotLabels;
        this.statusLabel = statusLabel;
    }

    @Override
    public void update(Observable observable, Object o) {
        AddressFactory addressFactory = spreadSheet.getAddressFactory();
        try {
            spreadSheet.clearSlot(addressFactory.create(markedSlot.getAddress()));                  //if this line fails because of a dependency on the cleared slot,
                                                                                                    //      spreadsheet puts the slot back, throws an exception and the
                                                                                                    //      view is left unchanged
            slotLabels.setLabel(addressFactory.stringToInt(markedSlot.getAddress()), "");
        } catch (XLException e) {
            statusLabel.setText(e.getMessage());
        }
    }
}
