package controller;

import gui.SlotLabels;
import model.Address;
import model.Slot;
import model.SpreadSheet;

import java.util.*;

/**
 * When the model is updated, this class updates the view based on the model.
 */
public class ModelObserver implements Observer {
    private SpreadSheet spreadSheet;
    private SlotLabels slotLabels;

    ModelObserver(SpreadSheet spreadSheet, SlotLabels slotLabels) {
        this.spreadSheet = spreadSheet;
        this.slotLabels = slotLabels;
    }

    //if the model is updated, every slot in the model is put into the view. note that it does not clear the view first.
    @Override
    public void update(Observable observable, Object o) {

        spreadSheet = (SpreadSheet) observable;
        Set slotsSet = spreadSheet.getSlots().entrySet();
        Iterator<Map.Entry<Address, Slot>> iterator = slotsSet.iterator();

        while (iterator.hasNext()){
            Map.Entry entry = iterator.next();
            int index = spreadSheet.getAddressFactory().addressToInt((Address)entry.getKey());
            slotLabels.setLabel(index, ((Slot)entry.getValue()).display(spreadSheet));
        }
    }
}
