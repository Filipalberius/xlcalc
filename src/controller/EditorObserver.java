package controller;

import gui.StatusLabel;
import gui.MarkedSlot;
import gui.Editor;
import gui.SlotLabels;
import model.SpreadSheet;
import model.Address;
import model.AddressFactory;
import model.SlotFactory;
import util.XLException;

import java.util.Observable;
import java.util.Observer;

/**
 * The observers of the graphical element Editor in an XL window. When the Editor is activated, its content is sent
 * to the model and the StatusLabel is updated.
 */
public class EditorObserver implements Observer {
    private SpreadSheet spreadSheet;
    private StatusLabel statusLabel;
    private MarkedSlot markedSlot;
    private SlotLabels slotLabels;

    EditorObserver(SpreadSheet spreadSheet, StatusLabel statusLabel, MarkedSlot markedSlot, SlotLabels slotLabels) {
        this.spreadSheet = spreadSheet;
        this.statusLabel = statusLabel;
        this.markedSlot = markedSlot;
        this.slotLabels = slotLabels;
    }

    @Override
    public void update(Observable observable, Object o) {
        Editor editor = (Editor) o;
        String editorText = editor.getText();

        SlotFactory sf = spreadSheet.getSlotFactory();
        AddressFactory af = spreadSheet.getAddressFactory();
        String addressTxt = markedSlot.getAddress();
        Address address = af.create(addressTxt);

        //if the user inputs an empty string and presses enter,
        //the selected slot is cleared from model and view.
        if(editorText.equals("")) {
            spreadSheet.clearSlot(address);
            slotLabels.setLabel(af.stringToInt(addressTxt), "");           
            return;
        }

        //if the input is valid, the model is updated. if the model throws an exception, it is caught and the
        //statuslabel is updated with the error message.
        try {
            spreadSheet.setSlot(sf.create(editor.getText()), address);
            editor.setText("");
            statusLabel.setText("");
        } catch (XLException error) {
            statusLabel.setText(error.getMessage());
        }
    }
}
