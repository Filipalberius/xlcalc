package model;

/**
 * Class Address represents a Key in the Map slots in SpreadSheet.
 * It consists of an upper case letter representing its column,
 * and a number representing its row.
 */
public class Address {

    private String address;

    Address(String address) {
        this.address = address;
    }

    /**
     * Overridden equals method to compare object by their inherent string.
     * @param other The Object to be compared
     * @return Boolean true if the Object equals this, or false if not.
     */
    @Override
    public boolean equals(Object other){

        if(this == other){
            return true;
        } else if (!(other instanceof Address)) {
            return false;
        } else {
            Address x = (Address) other;
            return this.address.equals(x.address);
        }
    }

    /**
     * Overridden hashCode method to make putting Addresses in a HashMap possible.
     * @return Hashcode of the inherent String.
     */
    @Override
    public int hashCode(){
       return address.hashCode();
    }

    /**
     * Returns the string of the instance.
     * @return Returns the string of the instance.
     */
    @Override
    public String toString() {
        return address;
    }
}
