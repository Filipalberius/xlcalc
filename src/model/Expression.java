package model;

import expr.Environment;
import expr.Expr;

/**
 * Class to represent an expression in the SpreadSheet.
 */
public class Expression implements Slot {
    private Expr expr;

    Expression(Expr expr) {
        this.expr = expr;
    }

    /**
     * Returns the Expression as a String.
     * @param environment The environment represented by SpreadSheet.
     * @return Returns the Expression as a String.
     */
    public String display(Environment environment) {
        return String.valueOf(expr.value(environment));
    }

    /**
     * Calculates the value of the Expression in the context of its environment Spreadsheet.
     * @param env The Expressions environment, SpreadSheet.
     * @return Returns the value of the Expression.
     */
    @Override
    public double getValue(Environment env) {
        return expr.value(env);
    }

    /**
     * Returns the Expression as a string.
     * @return Returns the Expression as a string.
     */
    public String getContent() {
        return expr.toString();
    }

    /**
     * Needed to be able to save Expressions in the SpreadSheet to a file.
     * @return Returns the Expression as a string.
     */
    @Override
    public String toString() {
        return getContent();
    }
}
