package model;

import expr.Environment;

class EmptySlot implements Slot {
    @Override
    public String getContent() {
        return "";
    }

    @Override
    public String display(Environment env) {
        return "";
    }

    @Override
    public double getValue(Environment env) {
        return 0;
    }
}
