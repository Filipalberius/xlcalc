package model;

import expr.Environment;

public interface Slot {    
    String getContent();
    
    String display(Environment env);
    
    double getValue(Environment env);
}