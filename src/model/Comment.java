package model;

import expr.Environment;

/**
 * Class to represent a comment in the SpreadSheet
 */
public class Comment implements Slot {
    private String comment;

    Comment(String comment) {
        this.comment = comment;
    }

    /**
     * Returns the comment as a String.
     * @param environment The environment represented by SpreadSheet
     * @return Returns the comment as a String.
     */
    public String display(Environment environment) {
        return comment;
    }

    /**
     * @param env
     * @return 0.0 as specified.
     */
    @Override
    public double getValue(Environment env) {
        return 0;
    }

    /**
     * Returns the comment as a string, with prepended by #.
     * @return Returns the comment as a string, with prepended by #.
     */
    public String getContent() {
        return '#' + comment;
    }

    /**
     * Needed to be able to save Coments in the SpreadSheet to a file.
     * @return Returns the comment as a string, with prepended by #.
     */
    @Override
    public String toString() {
        return getContent();
    }
}
