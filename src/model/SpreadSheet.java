package model;

import expr.Environment;
import util.XLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

/**
 * The class SpreadSheet models a SpreadSheet program like excel. It contains a Map which maps Slots to Addresses.
 * The Slots can contain either arithmetic expressions or comments. The value of each slot can be calculated.
 * SpreadSheet extends Observable so that it can have Observers in a GUI.
 * It also implements Environment to be able to access the arithmetic system of the package expr.
 */
public class SpreadSheet extends Observable implements Environment {

    private Map<Address, Slot> slots;
    private SlotFactory slotFactory;
    private AddressFactory addressFactory;

    /**
     * Constructor to create a SpreadSheet and its Address- and Slot Factory.
     * @param rows used by AddressFactory to correctly parse strings, or ints, to Addresses.
     * @param cols used by AddressFactory to correctly parse strings, or ints, to Addresses.
     */
    public SpreadSheet (int rows, int cols) {
        this.slots = new HashMap<>();
        this.addressFactory = new AddressFactory(rows, cols);
        this.slotFactory = new SlotFactory();
    }

    /**
     * Overridden method from the Environment interface.
     * It takes a String, representing an Address, and calculates the value of the corresponding Slot.
     * @param name String representing an Address
     * @return The value of the corresponding Slot
     * @throws XLException if an empty slot is referenced
     */
    @Override
    public double value(String name) {

        try {
            return slots.get(addressFactory.create(name)).getValue(this);
            } catch (NullPointerException e) {
            throw new XLException("An empty slot was referenced");
        }
    }

    /**
     * Checks evey Slot for errors.
     * @throws XLException if any Slot contains an error, like a circular dependency or a reference to an empty slot
     */
    public void checkAll() {
        for(Map.Entry<Address, Slot> mapEntry : slots.entrySet()) {
            Address address = mapEntry.getKey();
            Slot slot = mapEntry.getValue();

            addSlot(address, new Bomb());
            slot.getValue(this);
            addSlot(address, slot);
        }
    }

    /**
     * Method simplifies signaling to Observers.
     */
    public void setUpdated() {
        setChanged();
        notifyObservers();
    }

    /**
     * Removes the <Address, Slot> entry at the desired Address.
     * @param address The Address at which to remove entry
     * @throws XLException if Slot cannot be removed (another Slot depends on it)
     */
    public void clearSlot(Address address) {
        Slot slot = slots.remove(address);

        //tries to remove the slot, but if the slot can't be removed an exception is thrown and caught, putting the
        //slot back
        try {
            slots.remove(address);
            setUpdated();
        } catch (XLException e) {
            addSlot(address, slot);
            throw new XLException("A Slot depends on this slot; please remove dependency before clearing");
        }
    }

    /**
     * Clears the SpreadSheet by clearing the entire Map of entries.
     */
    public void clearAllSlots() {
        slots.clear();
        setUpdated();
    }

    /**
     * @return Returns the Map slots
     */
    public Map<Address, Slot> getSlots() {
        return slots;
    }

    /**
     * @return Returns the SpreadSheets instance of AddressFactory.
     */
    public AddressFactory getAddressFactory() {
        return this.addressFactory;
    }

    /**Returns the SlotFactory.
     * @return Returns the SpreadSheets instance of SlotFactory.
     */
    public SlotFactory getSlotFactory() {
        return this.slotFactory;
    }

    /** Returns a specific Slot, or an EmptySlot if there is none.
     * @param address The address corresponding to the desired slot.
     * @return Returns the Slot at the desired Address, or an EmptySlot.
     */
    public Slot getSlot(Address address) {
        return slots.getOrDefault(address, new EmptySlot());
    }

    /**
     * Adds a Slot to the specified Address (overrides the current if there already is one)
     * @param slot The Slot to be added to the SpreadSheet.
     * @param address The Address of the new Slot.
     * @throws XLException if the Slot cannot be put at the specified Address because it is invalid or it will
     * result in a circular dependency
     */
    public void setSlot(Slot slot, Address address) {
        addSlot(address, new Bomb());
        try {
            slot.getValue(this);
        } catch (XLException e){
            clearSlot(address);
            throw e;
        }
        addSlot(address, slot);
        setUpdated();
    }

    /**
     * Adds a Slot to the specified Address (overrides the current if there already is one). Does not check for errors,
     * and should therefore be used with caution.
     * @param slot The Slot to be added to the SpreadSheet.
     * @param address The Address of the new SLot.
     */
    public void setSlotUnsafe(Slot slot, Address address) {
        addSlot(address, slot);
    }

    private void addSlot(Address address, Slot slot) {
        slots.put(address, slot);
    }
}
