package model;

import expr.ExprParser;
import expr.Expr;
import util.XLException;
import java.io.IOException;

/**
 * Contains useful methods for Slots to be created and transformed.
 */
public class SlotFactory {
    ExprParser parser;
    
    SlotFactory() {
        this.parser = new ExprParser();
    }

    /**
     * Method for creating a Slot from a String.
     * @param input The String representing the Slot to be created.
     * @return Returns the SLot created from the String.
     */
    public Slot create(String input) {
        if (input.charAt(0) != '#') {
            try {
                Expr expr = parser.build(input);
                return new Expression(expr);
            } catch(IOException e) {
                throw new XLException("Error creating Slot in SlotFactory.");
            }
        } else {
            return new Comment(input.substring(1));
        }
    }
}