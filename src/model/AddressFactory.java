package model;

import util.XLException;

/**
 * Contains useful methods for Addresses to be created and transformed.
 */
public class AddressFactory {
    private int rows;
    private int cols;

    /**
     * Constructor for AddressFactory.
     * @param rows the number of rows of the SpreadSheet
     * @param cols the number of collumns of the SpreadSheet
     */
    AddressFactory(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
    }

    /**
     * Method for creating an Address from a String.
     * @param stringAddress The String representing the Address to be created.
     * @return Returns the Address created from the String.
     */
    public Address create(String stringAddress) {
        try {
            char col = stringAddress.toUpperCase().charAt(0);
            int row = Integer.parseInt(stringAddress.substring(1));

            if (!(col >= 'A' && col < 'A' + cols)) {
                throw new XLException("Column index is out of range");
            }

            if (row < 1 || row > rows) {
                throw new XLException("Row index is out of range");
            }

            return new Address(String.valueOf(col) + row);
        } catch (NumberFormatException e) {
            throw new XLException("Address must be of type 'CX', where C is a character and X is a number");
        }
    }

    /**
     * Creates an Address from an int. The int represents the index of a complete grid representation of a SpreadSheet.
     * @param address The Address to be converted to int.
     * @return Returns the int corresponding to the Address.
     */
    public int addressToInt(Address address){
        String stringAddress = address.toString();
        int col = ((byte)stringAddress.charAt(0) - 65);
        int row = Integer.parseInt(stringAddress.substring(1)) -1;
        return(cols*row + col);
    }

    /**
     * Converts a string representing an Address to an int representing the corresponding index of a
     * grid representation of SpreadSheet.
     * @param address The String representation of an address.
     * @return the int corresponding to the input string.
     */
    public int stringToInt(String address) {
        return addressToInt(create(address));
    }
}
