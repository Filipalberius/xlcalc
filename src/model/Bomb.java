package model;

import expr.Environment;
import util.XLException;

class Bomb implements Slot {
    private static final String MESSAGE = "Spreadsheet contains a circular dependency";

    public String display(Environment environment) {
        throw new XLException(MESSAGE);
    }

    @Override
    public double getValue(Environment env) {
        throw new XLException(MESSAGE);
    }

    public String getContent() {
        throw new XLException(MESSAGE);
    }
}
