package gui;

import java.awt.Color;
import java.util.Observable;
import javax.swing.JTextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Editor extends JTextField implements ActionListener {
    private ObservableAttribute observableAttribute;

    public Editor() {
        setBackground(Color.WHITE);
        addActionListener(this);
        observableAttribute = new ObservableAttribute();
    }

    public void actionPerformed(ActionEvent event) {
        observableAttribute.setChanged();
        observableAttribute.notifyObservers(this);
    }
    
    public Observable getObservable() {
        return observableAttribute;
    }
}