package gui;

import static java.awt.BorderLayout.CENTER;
import static java.awt.BorderLayout.NORTH;
import static java.awt.BorderLayout.SOUTH;
import gui.menu.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import javax.swing.JFrame;
import javax.swing.JPanel;

//many attributes were added, so outside packages can access the graphical elements and add functionality via getters
public class XL extends JFrame implements Printable {
    private XLCounter counter;
    private StatusLabel statusLabel = new StatusLabel();
    private Editor editor;
    private MarkedSlot markedSlot;
    private SlotLabels slotLabels;
    private XLList xlList;
    private LoadMenuItem loadMenuItem;
    private SaveMenuItem saveMenuItem;
    private ClearMenuItem clearMenuItem;
    private ClearAllMenuItem clearAllMenuItem;
    private NewMenuItem newMenuItem;
    private int rows;
    private int cols;

    public XL(XL oldXL) {
        this(oldXL.xlList, oldXL.counter, oldXL.rows, oldXL.cols);
    }

    public XL(XLList xlList, XLCounter counter, int rows, int cols) {
        super("Untitled-" + counter);
        this.xlList = xlList;
        this.counter = counter;
        this.rows = rows;
        this.cols = cols;
        xlList.add(this);
        counter.increment();

        markedSlot = new MarkedSlot();
        slotLabels = new SlotLabels(rows, cols, markedSlot);
        loadMenuItem = new LoadMenuItem(this, statusLabel);
        saveMenuItem = new SaveMenuItem(this, statusLabel);
        clearMenuItem = new ClearMenuItem();
        clearAllMenuItem = new ClearAllMenuItem();
        newMenuItem = new NewMenuItem();

        JPanel statusPanel = new StatusPanel(statusLabel, markedSlot);
        JPanel sheetPanel = new SheetPanel(rows, cols, markedSlot, slotLabels);
        editor = new Editor();
        add(NORTH, statusPanel);
        add(CENTER, editor);
        add(SOUTH, sheetPanel);
        setJMenuBar(new XLMenuBar(this, xlList, statusLabel, loadMenuItem, clearMenuItem, clearAllMenuItem, newMenuItem, saveMenuItem));
        pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setVisible(true);
    }

    public int print(Graphics g, PageFormat pageFormat, int page) {
        if (page > 0)
            return NO_SUCH_PAGE;
        Graphics2D g2d = (Graphics2D) g;
        g2d.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        printAll(g2d);
        return PAGE_EXISTS;
    }

    public void rename(String title) {
        setTitle(title);
        xlList.setChanged();
    }

    public Editor getEditor() {
        return editor;
    }

    public MarkedSlot getMarkedSlot() {
        return markedSlot;
    }

    public StatusLabel getStatusLabel() {
        return statusLabel;
    }

    public SlotLabels getSlotLabels() {
        return slotLabels;
    }

    public LoadMenuItem getLoadMenuItem() {
        return loadMenuItem;
    }

    public SaveMenuItem getSaveMenuItem() {
        return saveMenuItem;
    }

    public ClearMenuItem getClearMenuItem() {
        return clearMenuItem;
    }

    public ClearAllMenuItem getClearAllMenuItem() {
        return clearAllMenuItem;
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public NewMenuItem getNewMenuItem() {
        return newMenuItem;
    }
}