package gui;

import java.awt.Color;
import java.util.Observable;
import java.util.Observer;

public class CurrentLabel extends ColoredLabel implements Observer {
    public CurrentLabel(MarkedSlot markedSlot) {
        super("A1", Color.WHITE);
        markedSlot.addObserver(this);
    }

    @Override
    public void update(Observable observable, Object o) {
        setText(((MarkedSlot) observable).getAddress());
    }
}