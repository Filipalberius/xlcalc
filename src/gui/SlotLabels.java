package gui;

import java.awt.Color;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingConstants;

public class SlotLabels extends GridPanel {
    private List<SlotLabel> labelList;

    public SlotLabels(int rows, int cols, MarkedSlot markedSlot) {
        super(rows + 1, cols);

        labelList = new ArrayList<SlotLabel>(rows * cols);
        for (char ch = 'A'; ch < 'A' + cols; ch++) {
            add(new ColoredLabel(Character.toString(ch), Color.LIGHT_GRAY,
                    SwingConstants.CENTER));
        }
        for (int row = 1; row <= rows; row++) {
            for (char ch = 'A'; ch < 'A' + cols; ch++) {
                SlotLabel label = new SlotLabel();

                //if this label is pressed, labellist is linearly searched to find out what index this label has.
                //markedslot is then updated with a reference to this label and the string representation of the
                //index. this option was chosen instead of a label knowing its index to keep the memory usage down.
                label.addMouseListener(new MouseAdapter() {
                    public void mouseClicked(MouseEvent e) {
                        int index = labelList.indexOf(label);
                        int row = index / cols + 1;
                        char col = (char) (index % cols + 'A');
                        String address = String.valueOf(col) + row;

                        markedSlot.set(label, address);
                    }
                });

                add(label);
                labelList.add(label);
            }
        }

        markedSlot.set(labelList.get(0), "A1");
    }

    //updates a label
    public void setLabel(int index, String value){
        labelList.get(index).setText(value);
    }

    //clears every slotlabel in the attribute labellist
    public void clearAll() {
        for (SlotLabel slotLabel : labelList) {
            slotLabel.setText("");
        }
    }
}