package gui.menu;

import gui.ObservableAttribute;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

public class NewMenuItem extends JMenuItem implements ActionListener {
    private ObservableAttribute observableAttribute;

    public NewMenuItem() {
        super("New");
        this.observableAttribute = new ObservableAttribute();
        addActionListener(this);
    }

    public void actionPerformed(ActionEvent event) {
        observableAttribute.setChanged();
        observableAttribute.notifyObservers();
    }

    public ObservableAttribute getObservableAttribute() {
        return observableAttribute;
    }
}