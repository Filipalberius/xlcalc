package gui.menu;

import gui.ObservableAttribute;
import gui.StatusLabel;
import gui.XL;
import javax.swing.JFileChooser;

public class LoadMenuItem extends OpenMenuItem {
    private ObservableAttribute observableAttribute;
 
    public LoadMenuItem(XL xl, StatusLabel statusLabel) {
        super(xl, statusLabel, "Load");
        observableAttribute = new ObservableAttribute();
    }

    protected void action(String path) {
        observableAttribute.setChanged();
        observableAttribute.notifyObservers(path);
    }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showOpenDialog(xl);
    }

    public ObservableAttribute getObservableAttribute() {
        return observableAttribute;
    }
}