package gui.menu;

import gui.ObservableAttribute;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;

public class ClearMenuItem extends JMenuItem implements ActionListener {
    private ObservableAttribute observableAttribute;

    public ClearMenuItem() {
        super("Clear");
        observableAttribute = new ObservableAttribute();
        addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        observableAttribute.setChanged();
        observableAttribute.notifyObservers();
    }

    public ObservableAttribute getObservableAttribute() {
        return observableAttribute;
    }
}