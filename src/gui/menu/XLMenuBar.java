package gui.menu;

import gui.StatusLabel;
import gui.XL;
import gui.XLList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;

public class XLMenuBar extends JMenuBar {
    public XLMenuBar(XL xl, XLList xlList, StatusLabel statusLabel, LoadMenuItem loadMenuItem,
                     ClearMenuItem clearMenuItem, ClearAllMenuItem clearAllMenuItem, NewMenuItem newMenuItem,
                     SaveMenuItem saveMenuItem) {
        JMenu file = new JMenu("File");
        JMenu edit = new JMenu("Edit");
        file.add(new PrintMenuItem(xl, statusLabel));
        file.add(saveMenuItem);
        file.add(loadMenuItem);
        file.add(newMenuItem);
        file.add(new CloseMenuItem(xl, xlList));
        edit.add(clearMenuItem);
        edit.add(clearAllMenuItem);
        add(file);
        add(edit);
        add(new WindowMenu(xlList));
    }
}