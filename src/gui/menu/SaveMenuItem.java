package gui.menu;

import gui.ObservableAttribute;
import gui.StatusLabel;
import gui.XL;
import javax.swing.JFileChooser;

public class SaveMenuItem extends OpenMenuItem {
    private ObservableAttribute observableAttribute;

    public SaveMenuItem(XL xl, StatusLabel statusLabel) {
        super(xl, statusLabel, "Save");
        observableAttribute = new ObservableAttribute();
    }

    protected void action(String file) {
        observableAttribute.setChanged();
        observableAttribute.notifyObservers(file);
    }

    protected int openDialog(JFileChooser fileChooser) {
        return fileChooser.showSaveDialog(xl);
    }

    public ObservableAttribute getObservableAttribute() {
        return observableAttribute;
    }
}