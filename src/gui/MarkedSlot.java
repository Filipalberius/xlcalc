package gui;

import java.awt.*;
import java.util.Observable;

/**
 * Class remembering which slot of the gui is marked and active.
 */
public class MarkedSlot extends Observable {
    private SlotLabel marked;
    private String address;

    /**
     * @return Returns the Address of the active slot in the form of a String.
     */
    public String getAddress() {
        return address;
    }
    
    void set(SlotLabel slotLabel, String address) {
        if(marked != null) {
            marked.setBackground(Color.WHITE);
        }

        this.marked = slotLabel;
        this.address = address;
        slotLabel.setBackground(Color.YELLOW);

        setChanged();
        notifyObservers();
    }
}
