package gui;

import java.util.Observable;

/**
 * This class can function as a substitute for Observable: when a class already extends a class, it can't also
 * extend Observable. One solution is to use this as an attribute instead. It is used like Observable, but the
 * setChanged() method can be called from other classes instead of only by itself.
 */
public class ObservableAttribute extends Observable {

    @Override
    public void setChanged() {
        super.setChanged();
    }
}
