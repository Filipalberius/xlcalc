Frågor:

1. SpreadSheet, Slot, SlotFactory, Address, AddressFactory, Comment, 
Expression, Bomb, EmptySlot.

2. Varje plats ska innehålla en instans av en klass som implementerar 
interface Slot.

3. Lagra alla Slot i en HashMap.

4. De klasser som ska observeras är:
SpreadSheet,
ClearAllMenuItem,
ClearMenuItem,
LoadMenuItem,
NewMenuItem,
SaveMenuItem,
Editor,
MarkedSlot,
XLList,

De klasser som ska observera är:
WindowMenu,
CurrentLabel,
StatusLabel,
ClearAllMenuItemObserver,
ClearMenuItemObserver,
EditorObserver,
LoadMenuItemObserver,
MarkedSlotObserver,
ModelObserver,
NewMenuItemObserver,
SaveMenuItemObserver,


5. Klassen MarkedSlot i paketet gui

6. Den enbart estetiska delen av gui-paketet är färdig. Ingenting i detta 
fungerar dock att interagera med förutom menyelementen. Dessa är dock inte
heller i sin tur kopplade till någon funktion. Aritmetiken som Modellen ska
bygga på är också färdig

7. Majoriteten av felen kastas av modellen och fångas av controllern, så att
användaren notifieras på rätt sätt via vyn.

8. Variable

9. SpreadSheet ska implementera Environment. Anledningen till att man använder
gränssnittet istället för klassnamnet är för att man enkelt ska kunna använda
flera implementeringar av samma gränssnitt som gör samma sak.

10. En bomb placeras först på platsen i fråga, vapå värdet rekursivt provas
att beräknas. Om bomben smäller kastas ett fel, annars placeras värdet

Användningsfall:

1.  Mata in värden. Man ska kunna markera en ruta i arket och antingen välja
att skriva en kommentar genom att påbörja inmatningen med #, eller lägga in ett
värde. Efter det ska arket uppdateras med med antingen ett värde eller en
kommentar i det markerade fältet.
Ett fel som kan inträffa är att användaren skriver ett syntaxfel. Då skriver
programmet ut ett förklarande felmeddelande i StatusLabel.

2.  Ta bort värden från ett fält. Fältet markeras, i editmenyn väljs att göra
nuvarande fält tom varpå fältet blir tommt.

3.  Tömma hela arket på värden. Man ska kunna välja "clear" i menyn varpå hela
arket ska tömmas på värden.

4.  Ladda in en fil. Man ska i menyn kunna välja att ladda in ett sparat ark
i programmet från en lokal fil.
Ett fel som kan hända är att användaren försöker ladda in ett dokument som inte
finns. Då skriver programmet ut ett förklarande felmeddelande i StatusLabel.

5.  Spara en fil. Man ska kunna spara det nuvarande arket till en lokal fil
genom att välja "save" i menyn.

6.  Skapa en ny fil. Om man väljer att skapa ett nytt ark i menyn ska det
nuvarande arket finnas kvar att välja under menyn "windows". 
 
7.  Man ska kunna byta mellan olika aktiva ark från menyn "windows".

8.  Skriva ut ett ark. I File-menyn väljs Print. En print-dialog öppnas och det
nuvarnade arket kan skrivas ut.
  
Expr:
![alt text](uml/expr.jpeg)

Gui:
![alt text](uml/gui.jpeg)

Menu:
![alt text](uml/gui.menu.jpeg)

Model:
![alt text](uml/model.jpeg)

Controller:
![alt text](uml/controller.jpeg)

Util:
![alt text](uml/util.jpeg)

Tidsåtgång:
Emil: 33 h
Filip: 33 h
Thomas: 31 h
Elmer: 17 h
